<?php 
    session_start();

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        //regardles of the content the user will be autenticated
        $_SESSION["authenticated"] = TRUE;
        $_SESSION["funds"] = 1000;
        $_SESSION['history']=[];
        header("Location: index.php");
    }

    include 'header.php';
?>
    <!-- about part start-->
    <section class="about_part mt-5">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-8">
                    <form action="login.php" method="POST">
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control" id="email" aria-describedby="emailHelp" value="notsosecure@example.com">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" value="p@ssw0rd">
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1" checked>
                            <label class="form-check-label" for="exampleCheck1">Check this box to receive unlimited funds!</label>
                        </div>
                        <button type="submit" class="btn btn-warning btn-lg px-5" style="color:white" >Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php include 'footer.php'; ?>