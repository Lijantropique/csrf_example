<?php 
    session_start();
    $msg = '';
    $purchase = FALSE;
    if ($_SERVER['REQUEST_METHOD'] === 'POST' &&  isset($_SESSION["authenticated"]) ) {
        $purchase = TRUE;
        array_push($_SESSION['history'],[$_POST['total'],$_POST['address']]);
        // print_r($_POST);
        $_SESSION['funds'] -= $_POST['total'];
        $msg .= 'You just bought items for $' .$_POST['total'] .'. They will be delivered to '. $_POST['address'];
    }
    include 'header.php';
?>
    <!-- about part start-->
    <section class="about_part">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-4 col-lg-5 offset-lg-1">
                    <div class="about_img">
                        <img src="img/about.png" alt="">
                    </div>
                </div>
                <div class="col-sm-8 col-lg-4">
                    <div class="about_text">
                        
                        <!-- authenticated users start-->
                        <?php if(isset($_SESSION["authenticated"])): ?>
                        <h2>Your awesome and super expensive food is ready!</h2>
                        <?php 
                            $products = array(
                                "Baked Crispy Dodo legs"=>["A Mauritius' deli. Try them before it is too late ...", "90"], 
                                "toMacco basket"=>["Homer's secret recipe. About 20 units per basket", "35"], 
                                "slurm 6-pack"=>["Fry's nightmare. Taste the queen!","10"]
                            );
                            $total=0.0;
                        ?>
                        <ul class="list-group mb-3">
                            <?php foreach ($products as $product => $args):?>
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <h6 class="my-0"><?php echo $product;?></h6>
                                    <small class="text-muted"><?php echo $args[0];?></small>
                                </div>
                                <span class="text-muted">$<?php echo $args[1];?></span>
                                </li>
                                <?php $total += (float)$args[1];?>
                            <?php endforeach;?>
                            <li class="list-group-item d-flex justify-content-between bg-light">
                            <div class="text-success">
                                <h6 class="my-0">Promo code</h6>
                                <small>EXAMPLECODE</small>
                                <?php $total -= 5.0;?>
                            </div>
                            <span class="text-success">-$5</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between">
                            <span>Total (USD)</span>
                            <strong>$<?php echo ($total);?></strong>
                            </li>
                        </ul>

                        <h4> --Verify your shipping address-- </h4>
                        <form class="needs-validation" novalidate action="index.php" method="POST">
                            <input type="hidden" name="total" id="hiddenField" value="<?php echo $total; ?>" />
                            <div class="mb-3">
                            <label for="address">Address</label>
                            <input type="text" name="address" class="form-control" id="address" placeholder="1234 Main St" required>
                            <div class="invalid-feedback">
                                Please enter your shipping address.
                            </div>
                            </div>
                            <hr class="mb-4">
                            <button class="btn btn-warning btn-lg btn-block text-white" type="submit">Proceed!</button>
                        </form>

                        <!-- authenticated users end-->
                        <?php else:?>
                        <h2>You must log in before you can finalize your purchase</h2>
                        <form action="login.php">
                        <button class="btn btn-warning btn-lg px-5 text-white" type="submit">Log in</button>
                        </form>
                        <?php endif;?>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    
<?php include 'footer.php'; ?>