<?php 
    session_start();
    include 'header.php';
?>
    <!-- about part start-->
    <section class="about_part mt-5 p-0 pb-4">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-8">
                    <h3 class="">Return to the main page</h3>

                    <table class="table table-hover">
                        <thead>
                            <tr>
                            <th scope="col">Total Cost</th>
                            <th scope="col">Shipping Address</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($_SESSION['history'] as $transaction):?>
                            <tr>
                                <td><?php echo $transaction[0] ; ?></td>
                                <td><?php echo $transaction[1] ; ?></td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

<?php include 'footer.php'; ?>